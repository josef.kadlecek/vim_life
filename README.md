# VIM

# Prerequisities 
Have npm installed (for CoC plugin - autocomplete) 

Install vim plug. 
`
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
`


# Setup 

Copy the contents of .vimrc into your .vimrc.

Put monokai.vim into ~/.vim/colors/monokai.vim

Run plugin install inside vim
`
:PlugInstall
`

